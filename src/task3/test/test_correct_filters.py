import pytest
import pandas as pd
from math import isclose
from src.task3.lib.filters import (SMAFilter, EMAFilter, MedianFilter)

DATA = [5.3, 6.7, 7.9, 7.1, 5.2, 4.1, 3.5, 5.4,
        7.3, 9.4, 8.0, 6.6, 7.9, 9.2, 7.6]
WINDOW_SIZE = 4

df = pd.DataFrame(DATA)

params = [(SMAFilter(DATA, WINDOW_SIZE).filter(), df.rolling(window=WINDOW_SIZE).mean().iloc[:, 0].tolist()),
          (EMAFilter(DATA, WINDOW_SIZE).filter(), [None, None, None, 6.75, 6.13, 5.318, 4.5908, 4.914479999999999,
                                                   5.868687999999999, 7.2812128, 7.568727679999999, 7.181236607999999,
                                                   7.4687419647999995, 8.161245178879998, 7.9367471073279985]),
          (MedianFilter(DATA, WINDOW_SIZE).filter(), [None, None, None, 6.9, 6.9, 6.15, 4.65, 4.65, 4.75, 6.35,
                                                      7.65, 7.65, 7.95, 7.95, 7.75])]


@pytest.mark.parametrize("test_input, excepted", params)
def test_correct_filters(test_input, excepted):
    for number in range(WINDOW_SIZE, len(DATA)):
        assert isclose(test_input[number], excepted[number], abs_tol=1.e-2)
