from random import uniform


class MonteCarloPI(object):
    def __init__(self, amount):
        self.amount = amount

    def simulate(self):
        inside = 0

        for i in range(self.amount):
            x = uniform(-1, 1)
            y = uniform(-1, 1)

            if x**2 + y**2 <= 1:
                inside += 1

        return 4*inside/self.amount
