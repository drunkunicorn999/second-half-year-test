from src.task1.lib.integrals_method import (LeftRectangles, RightRectangles,
                                            MiddleRectangles, Trapezium, Simpson)


class Integral(object):
    _methods = {
        0: LeftRectangles,
        1: RightRectangles,
        2: MiddleRectangles,
        3: Trapezium,
        4: Simpson
    }

    def __init__(self, function):
        self.__function = function

    @property
    def function(self):
        return self.__function

    @function.setter
    def function(self, new_function):
        __function = new_function

    def run(self, a, b, steps, method_index=0):
        h_step = (b-a)/steps
        method = self._methods.get(method_index)
        return h_step * sum(
            method(self.function, a + i*h_step, h_step).integrate() for i in range(steps))
