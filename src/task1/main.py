from src.task1.const_values import function, str_function
from src.task1.lib.callable_Integral import Integral


def main():
    print(f"Example function (a=10): {str_function}")

    a, b, steps = 0, 0, 0

    try:
        a, b, steps = map(int, input("\nEnter separated by space int - a, b, steps: ").split())
    except TypeError:
        print("Incorrect values!!!")

    if a > b:
        a, b = b, a
        print("Since a> b, they will swap values with each other")

    print("\n---------------LeftRectanglesRule---------------")
    print(f"Value = {Integral(function).run(a, b, steps)}")

    print("\n---------------RightRectanglesRule---------------")
    print(f"Value = {Integral(function).run(a, b, steps, method_index=1)}")

    print("\n---------------MiddleRectanglesRule---------------")
    print(f"Value = {Integral(function).run(a, b, steps, method_index=2)}")

    print("\n---------------TrapeziumRule---------------")
    print(f"Value = {Integral(function).run(a, b, steps, method_index=3)}")

    print("\n---------------SimpsonRule---------------")
    print(f"Value = {Integral(function).run(a, b, steps, method_index=4)}")


if __name__ == "__main__":
    main()
